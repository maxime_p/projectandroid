package com.emn.mperocheau;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class LibraryActivity extends AppCompatActivity implements ListBooksFragment.OnItemClickListener {

    private boolean landscape;
    private Book savedBook;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_library);

        landscape = getResources().getBoolean(R.bool.landscape);
        getSupportFragmentManager().popBackStack();

        if(landscape){
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.containerFrameLayout, new ListBooksFragment())
                    .commit();

            if(null != savedInstanceState){


                Book book = savedInstanceState.getParcelable("book");

                if(book != null){

                    DetailsFragment bookFragment = new DetailsFragment();
                    Bundle args = new Bundle();
                    args.putParcelable("book", book);
                    bookFragment.setArguments(args);

                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragmentDetail, bookFragment)
                            .addToBackStack(ListBooksFragment.class.getSimpleName())
                            .commit();
                } else {
                    findViewById(R.id.fragmentDetail).setVisibility(View.INVISIBLE);
                }
            } else {
                findViewById(R.id.fragmentDetail).setVisibility(View.INVISIBLE);
            }
        } else {

            if(null != savedInstanceState){

                Book book = savedInstanceState.getParcelable("book");

                if(book != null){

                    DetailsFragment bookFragment = new DetailsFragment();
                    Bundle args = new Bundle();
                    args.putParcelable("book", book);
                    bookFragment.setArguments(args);

                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.containerFrameLayout, bookFragment)
                            .addToBackStack(ListBooksFragment.class.getSimpleName())
                            .commit();

                } else {

                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.containerFrameLayout, new ListBooksFragment())
                            .commit();

                    findViewById(R.id.fragmentDetail).setVisibility(View.GONE);
                }
            } else {

                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.containerFrameLayout, new ListBooksFragment())
                        .commit();

                findViewById(R.id.fragmentDetail).setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onClick(Book book) {

        savedBook = book;

        DetailsFragment bookFragment = new DetailsFragment();
        Bundle args = new Bundle();
        args.putParcelable("book", book);
        bookFragment.setArguments(args);

        if(landscape){
            findViewById(R.id.fragmentDetail).setVisibility(View.VISIBLE);

            getSupportFragmentManager().popBackStack();

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.containerFrameLayout, new ListBooksFragment())
                    .commit();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragmentDetail, bookFragment)
                    .addToBackStack(ListBooksFragment.class.getSimpleName())
                    .commit();
        } else {

            getSupportFragmentManager().beginTransaction()
                .replace(R.id.containerFrameLayout, bookFragment)
                .addToBackStack(ListBooksFragment.class.getSimpleName())
                .commit();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if(savedBook != null && !landscape){
            savedBook = null;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if(savedBook != null) {

            outState.putParcelable("book", savedBook);
        }
    }

    public void setLandscape(boolean landscape) {
        this.landscape = landscape;
    }
}
