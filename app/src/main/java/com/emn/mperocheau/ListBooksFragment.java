package com.emn.mperocheau;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;
import timber.log.Timber;

public class ListBooksFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private OnItemClickListener listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (OnItemClickListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_listbooks, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new GridLayoutManager(view.getContext(),1);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new BookRecycledAdapter(getBooks(), listener);
        mRecyclerView.setAdapter(mAdapter);
        return view;
    }

    public interface OnItemClickListener {

         void onClick(Book book);
    }

    private List<Book> getBooks() {
        final ArrayList<Book> books = new ArrayList<>();

        Timber.plant(new Timber.DebugTree());

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://henri-potier.xebia.fr/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        BookService service = retrofit.create(BookService.class);

        Call<List<Book>> call = service.listBooks();

        call.enqueue(new Callback<List<Book>>() {
            @Override
            public void onResponse(Response<List<Book>> response, Retrofit retrofit) {
                for(Book book : response.body()){
                    Timber.i(book.getTitle());
                    books.add(new Book(book.getIsbn(), book.getTitle(), book.getPrice(), book.getCover()));
                }
                mAdapter.notifyDataSetChanged();
            }
            @Override
            public void onFailure(Throwable t) {
                Timber.e(t, "failed");
            }
        });
        return books;
    }
}
