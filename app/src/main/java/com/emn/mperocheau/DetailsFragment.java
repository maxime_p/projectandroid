package com.emn.mperocheau;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class DetailsFragment extends Fragment {

    private Book book;
    private TextView nameTextView;
    private ImageView imageImageView;
    private TextView priceTextView;
    private TextView isbnTextView;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(this.getArguments() != null){
            book = (Book) this.getArguments().get("book");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_details, container, false);

        if(book != null){

            nameTextView = (TextView) view.findViewById(R.id.detailNameTextView);
            imageImageView = (ImageView) view.findViewById(R.id.detailImageImageView);
            priceTextView = (TextView) view.findViewById(R.id.detailPriceTextView);
            isbnTextView = (TextView) view.findViewById(R.id.detailIsbnTextView);
        }

        return view;
    }

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(book != null) {

            nameTextView.setText("Titre : "+book.getTitle());
            priceTextView.setText("Prix : "+book.getPrice()+"€");
            isbnTextView.setText("ISBN : "+book.getIsbn());
            Glide.with(this).load(book.getCover()).into(imageImageView);
        }
    }

}
