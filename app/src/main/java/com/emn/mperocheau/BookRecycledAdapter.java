package com.emn.mperocheau;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Created by SPEROMAX on 22/01/2016.
 */

public class BookRecycledAdapter extends RecyclerView.Adapter<BookRecycledAdapter.ViewHolder> {
    private List<Book> books;
    private View view;
    private ListBooksFragment.OnItemClickListener listener;


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        private TextView nameTextView;
        private ImageView imageImageView;
        private TextView priceTextView;
        private CardView cardView;


        public ViewHolder(View v) {
            super(v);
            nameTextView = (TextView) v.findViewById(R.id.nameTextView);
            imageImageView = (ImageView) v.findViewById(R.id.imageImageView);
            priceTextView = (TextView) v.findViewById(R.id.priceTextView);
            cardView = (CardView) v.findViewById(R.id.card_view);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public BookRecycledAdapter(List<Book> myDataset, ListBooksFragment.OnItemClickListener listener) {
        this.books = myDataset;
        this.listener = listener;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public BookRecycledAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_view_item_book, parent, false);
        // set the view's size, margins, paddings and layout parameters
//        ...
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final ImageView imageView = (ImageView) view.findViewById(R.id.imageImageView);
        Glide.with(holder.itemView.getContext()).load(books.get(position).getCover()).into(imageView);

        holder.nameTextView.setText(books.get(position).getTitle());
        holder.priceTextView.setText(String.valueOf(books.get(position).getPrice())+" €");

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //implement onClick
//                Toast.makeText(holder.itemView.getContext(), books.get(position).getTitle(), Toast.LENGTH_SHORT).show();
                listener.onClick(books.get(position));
            }
        });

    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return books.size();
    }
}