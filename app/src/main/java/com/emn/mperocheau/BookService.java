package com.emn.mperocheau;

import java.util.List;

import retrofit.Call;
import retrofit.http.GET;

public interface BookService {

    @GET("books")
    Call<List<Book>> listBooks();

}
