package com.emn.mperocheau;

import org.assertj.android.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;

/**
 * Created by SPEROMAX on 12/02/2016.
 */
@RunWith(CustomRobolectricTestRunner.class)
public class LibraryActivityTest {

    LibraryActivity activity;

    @Before
    public void setUp() throws Exception {
        activity = Robolectric.setupActivity(LibraryActivity.class);
    }

    @Test
    public void testFragmentsAccueil() throws Exception {

        Assertions.assertThat(activity.findViewById(R.id.containerFrameLayout)).isVisible();
        Assertions.assertThat(activity.findViewById(R.id.fragmentDetail)).isNotVisible();
    }

    @Test
    public void testClickDetail() throws Exception {

       /* Book book = new Book("c8fabf68-8374-48fe-a7ea-a00ccd07afff",
                "Henri Potier à l'école des sorciers",
                "35",
                "Henri Potier à l'école des sorciers");

        activity.onClick(book);
        Assertions.assertThat(activity.findViewById(R.id.containerFrameLayout)).isNotVisible();
        Assertions.assertThat(activity.findViewById(R.id.fragmentDetail)).isVisible();*/
    }

    @Test
    public void testFragmentsAccueilLandscape() throws Exception {

        activity.setLandscape(true);
        Assertions.assertThat(activity.findViewById(R.id.containerFrameLayout)).isVisible();
        Assertions.assertThat(activity.findViewById(R.id.fragmentDetail)).isNotVisible();
    }

    @Test
    public void testClickDetailLandscape() throws Exception {

       /* activity.setLandscape(true);

        Book book = new Book("c8fabf68-8374-48fe-a7ea-a00ccd07afff",
                "Henri Potier à l'école des sorciers",
                "35",
                "Henri Potier à l'école des sorciers");

        activity.onClick(book);
        Assertions.assertThat(activity.findViewById(R.id.containerFrameLayout)).isVisible();
        Assertions.assertThat(activity.findViewById(R.id.fragmentDetail)).isVisible();*/
    }
}
